Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
	- it has two additional FINER logs
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
	- JUnit tests log themselves using the single logger available. The logger.properties file has the console only show INFO level logs, whereas the file logger shows ALL levels of logs
1.  What does Assertions.assertThrows do?
	- It catches an expected exception
1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
	- It allows Java to convert objects into streams of bytes to write to disk or send on the network. The serialVersionUID is used to manually set the serialisation number, which is used to verify that the sender and receiver of a serialized object have loaded classes for that object that are compatible with respect to serialization.
    2.  Why do we need to override constructors?
	- This way it creates a TimerException rather than an instance of its parent, Exception
    3.  Why we did not override other Exception methods?
	- The other exception methods do not have parameters to pass up the chain of inheritance
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	- The static block is only read when the class is loaded into memory (in this case, when the first static method in Timer.java is called). Inside the static block, the logger properties are read in.
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	- It is a way of formatting text similar to HTML that bitbucket uses for its readme files and pull requests' descriptions and comments
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	- I removed the "finally" after the try catch block as this was telling the system that all the exceptions had been handled
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	- the system thought all exceptions were handled due to the "finally" block
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException
	- TimerException is a decisionException (which is a runtime exception). NullpointerException is a runtime exception.
1.  Push the updated/fixed source code to your own repository.